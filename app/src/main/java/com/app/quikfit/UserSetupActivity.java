package com.app.quikfit;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.quikfit.orm.StepsViewModel;
import com.app.quikfit.orm.User;

public class UserSetupActivity extends AppCompatActivity {

    private EditText mEditHeightView;
    private EditText mEditWeightView;
    private Spinner mSpinnerHeight;
    private Spinner mSpinnerWeight;
    private Spinner mSpinnerGender;
    private StepsViewModel stepsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEditHeightView = findViewById(R.id.textView_height);
        mEditWeightView = findViewById(R.id.textView_weight);
        mSpinnerHeight = findViewById(R.id.height_types_spinner);
        mSpinnerWeight = findViewById(R.id.weight_types_spinner);
        mSpinnerGender = findViewById(R.id.textView_gender);
        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);

        Button mConfirm = findViewById(R.id.bt_confirm);
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                //Female Default
                boolean gender = false;
                boolean imperial = false;
                double height = Double.parseDouble(mEditHeightView.getText().toString());
                double weight = Double.parseDouble(mEditWeightView.getText().toString());
                double stride_length = Math.round(height * 0.413);

                if(mSpinnerGender.getSelectedItem().toString() == getResources().getStringArray(R.array.list_gender)[0])
                {
                    gender = true;
                    stride_length = Math.round(height * 0.415);
                }

                if(mSpinnerHeight.getSelectedItem().toString() == getResources().getStringArray(R.array.stride_measure_types)[1]) {
                    height = Math.round(height * 2.54);
                    stride_length = Math.round(stride_length * 2.54);
                    imperial = true;
                }
                if(mSpinnerWeight.getSelectedItem().toString() == getResources().getStringArray(R.array.weight_measure_types)[1]) {
                    weight = Math.round(weight / 0.4536);
                }

                stepsViewModel.insertUserInfo(new User(height, weight, gender, imperial, stride_length, 1));
                Toast.makeText(getApplicationContext(), "Details Saved.", Toast.LENGTH_LONG).show();
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }

}
