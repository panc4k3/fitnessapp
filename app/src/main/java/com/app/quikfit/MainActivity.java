package com.app.quikfit;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.quikfit.orm.Steps;
import com.app.quikfit.orm.StepsViewModel;
import com.app.quikfit.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private StepsViewModel stepsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);

        if(getResources().getBoolean(R.bool.user_info_set))
        {
            Intent intent = new Intent(MainActivity.this, UserSetupActivity.class);
            startActivity(intent);
        }

        if (isKitkatWithStepSensor()) {
            registerStepCounter(5);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterStepCounter();
    }

    private int mSteps = 0;
    private int mCounterSteps = 0;
    private int mPreviousCounterSteps = 0;

    private boolean isKitkatWithStepSensor() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            return true;
        }
        return false;
    }

    public void registerStepCounter(int maxDelay) {
//        mCounterSteps = 0;
//        SensorManager sensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
//        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
//        sensorManager.registerListener(mListener, sensor, SensorManager.SENSOR_DELAY_NORMAL, maxDelay);

        SensorManager sensorManager = (SensorManager) getSystemService((Activity.SENSOR_SERVICE));
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        sensorManager.registerListener(mListener, sensor, SensorManager.SENSOR_DELAY_NORMAL, maxDelay);
    }

    private void unregisterStepCounter() {
        SensorManager sensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
        sensorManager.unregisterListener(mListener);
    }

    private void resetCounter() {
        mSteps = 0;
        mCounterSteps = 0;
        mPreviousCounterSteps = 0;
    }

    private final SensorEventListener mListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
                if (mCounterSteps < 1) {
                    mCounterSteps = (int) event.values[0];
                }
            } else if(event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
                mCounterSteps = 0;
            }

            mSteps = (int) event.values[0];

            mPreviousCounterSteps += mSteps;

            if(mPreviousCounterSteps > 5)
            {
                stepsViewModel.insertSteps(new Steps(mPreviousCounterSteps));
                resetCounter();
            }
            Log.i("STEPS", "Steps detected: " + mPreviousCounterSteps);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
}