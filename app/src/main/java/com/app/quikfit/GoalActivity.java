package com.app.quikfit;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.quikfit.orm.Goal;
import com.app.quikfit.orm.StepsViewModel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class GoalActivity extends AppCompatActivity {

    private Spinner mGoalType;
    private EditText mGoalValue;
    private DatePicker mGoalDate;
    private TimePicker mGoalTime;

    private StepsViewModel stepsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal);

        mGoalType = findViewById(R.id.goal_type);
        mGoalDate = findViewById(R.id.goal_date);
        mGoalTime = findViewById(R.id.goal_time);
        mGoalValue = findViewById(R.id.goal_value);

        Date date = new Date();
        mGoalDate.setMinDate(date.getTime());

        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);
    }

    public void addNewGoal(View view)
    {
        Calendar goalDate = new Calendar.Builder().setDate(mGoalDate.getYear(), mGoalDate.getMonth(), mGoalDate.getDayOfMonth()).setTimeOfDay(mGoalTime.getHour(), mGoalTime.getMinute(), 0).build();
        Date current = new Date();
        Log.i("DATE", "Goal: " + goalDate.getTime().toString());
        Log.i("DATE", "Current: " + current.toString());
        if(goalDate.getTime().after(current)) {
            Goal goal = new Goal(mGoalType.getSelectedItem().toString(), Integer.parseInt(mGoalValue.getText().toString()), goalDate.getTime());
            stepsViewModel.insertGoal(goal);
            Toast.makeText(GoalActivity.this, "Goal Added", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(GoalActivity.this, "Invalid Date/Time", Toast.LENGTH_LONG).show();
        }
    }
}
