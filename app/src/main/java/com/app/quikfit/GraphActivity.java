package com.app.quikfit;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.app.quikfit.orm.Steps;
import com.app.quikfit.orm.StepsViewModel;
import com.app.quikfit.orm.User;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GraphActivity extends AppCompatActivity {

    BarChart chart;
    BarData mBarData;
    BarDataSet mBarDateSet;

    String type;

    ArrayList<BarEntry> mBarEntries;
    ArrayList<String> mBarLabels;

    TextView graphType;

    StepsViewModel stepsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);

        chart = findViewById(R.id.graph);
        graphType = findViewById(R.id.graph_type_text);

        switch(getIntent().getIntExtra("GRAPH_TYPE", 0))
        {
            case 0:
                setGraphWeight();
                break;

            case 1:
                setGraphStepsWeek();
                break;
        }
    }

    void setGraphWeight()
    {
        type = "Weight";
        graphType.setText(type);

        mBarEntries = new ArrayList<>();
        mBarLabels = new ArrayList<>();

        stepsViewModel.getAllUserInfo().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                for (int i = 0; i < users.size(); i++) {
                    mBarEntries.add(new BarEntry((float) users.get(i).weight, i));
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(users.get(i).timestamp.getTime());
                    mBarLabels.add((cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US)));
                }

                mBarDateSet = new BarDataSet(mBarEntries, type);
                mBarData = new BarData(mBarLabels, mBarDateSet);
                mBarDateSet.setColors(new int[] {
                        getResources().getColor(R.color.color_one, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_two, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_three, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_four, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_five, getApplicationContext().getTheme())
                });

                chart.setData(mBarData);
                chart.animateY(3000);
            }
        });
    }

    void setGraphStepsWeek()
    {
        type = "Steps This Week";
        graphType.setText(type);

        mBarEntries = new ArrayList<>();
        mBarLabels = new ArrayList<>();

        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.add(Calendar.DATE, -6);

        stepsViewModel.getStepsBetween(start.getTime(), Calendar.getInstance().getTime()).observe(this, new Observer<List<Steps>>() {
            @Override
            public void onChanged(@Nullable List<Steps> steps) {
                float mStepsOne = 0;
                float mStepsTwo = 0;
                float mStepsThree = 0;
                float mStepsFour = 0;
                float mStepsFive = 0;
                float mStepsSix = 0;
                float mStepsSeven = 0;

                int day_one = end.get(Calendar.DAY_OF_WEEK);
                String day_one_text = end.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar two = end;
                two.add(Calendar.DATE, -1);
                int day_two = two.get(Calendar.DAY_OF_WEEK);
                String day_two_text = two.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar three = end;
                three.add(Calendar.DATE, -1);
                int day_three = three.get(Calendar.DAY_OF_WEEK);
                String day_three_text = three.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar four = end;
                three.add(Calendar.DATE, -1);
                int day_four = four.get(Calendar.DAY_OF_WEEK);
                String day_four_text = four.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar five = end;
                three.add(Calendar.DATE, -1);
                int day_five = five.get(Calendar.DAY_OF_WEEK);
                String day_five_text = five.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar six = end;
                three.add(Calendar.DATE, -1);
                int day_six = six.get(Calendar.DAY_OF_WEEK);
                String day_six_text = six.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                int day_seven = start.get(Calendar.DAY_OF_WEEK);
                String day_seven_text = start.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);

                Calendar cal = Calendar.getInstance();

                for(int i = 0; i < steps.size(); i++) {
                    cal.setTimeInMillis(steps.get(i).timestamp.getTime());

                    if (cal.get(Calendar.DAY_OF_WEEK) == day_one){
                        mStepsOne += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_two){
                        mStepsTwo += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_three){
                        mStepsThree += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_four){
                        mStepsFour += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_five){
                        mStepsFive += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_six){
                        mStepsSix += steps.get(i).getSteps();
                    }
                    else if (cal.get(Calendar.DAY_OF_WEEK) == day_seven){
                        mStepsSeven += steps.get(i).getSteps();
                    }
                }

                mBarEntries.add(new BarEntry((float) mStepsSeven, 0));
                mBarEntries.add(new BarEntry((float) mStepsSix, 1));
                mBarEntries.add(new BarEntry((float) mStepsFive, 2));
                mBarEntries.add(new BarEntry((float) mStepsFour, 3));
                mBarEntries.add(new BarEntry((float) mStepsThree, 4));
                mBarEntries.add(new BarEntry((float) mStepsTwo, 5));
                mBarEntries.add(new BarEntry((float) mStepsOne, 6));

                mBarLabels.add(day_seven_text);
                mBarLabels.add(day_six_text);
                mBarLabels.add(day_five_text);
                mBarLabels.add(day_four_text);
                mBarLabels.add(day_three_text);
                mBarLabels.add(day_two_text);
                mBarLabels.add(day_one_text);

                mBarDateSet = new BarDataSet(mBarEntries, type);
                mBarData = new BarData(mBarLabels, mBarDateSet);
                mBarDateSet.setColors(new int[] {
                        getResources().getColor(R.color.color_one, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_two, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_three, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_four, getApplicationContext().getTheme()),
                        getResources().getColor(R.color.color_five, getApplicationContext().getTheme())
                });

                chart.setData(mBarData);
                chart.animateY(3000);
            }
        });
    }
}
