package com.app.quikfit.orm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StepsViewModel extends AndroidViewModel {

    private StepsRepository mStepsRepo;
    private LiveData<List<Steps>> mAllSteps;
    private LiveData<List<User>> mAllUserInfo;
    private LiveData<User> mUser;
    private LiveData<List<Activity>> mActivities;
    private LiveData<List<Goal>> mGoals;

    public StepsViewModel(Application application) {
        super(application);
        mStepsRepo = new StepsRepository(application);
        mAllSteps = mStepsRepo.getAllSteps();
        mAllUserInfo = mStepsRepo.getAllUserInfo();
        mUser = mStepsRepo.isFirstUser();
        mActivities = mStepsRepo.getAllActivities();
        mGoals = mStepsRepo.getAllGoals();
    }

    public LiveData<List<Steps>> getAllSteps() {
        return mAllSteps;
    }
    public LiveData<List<User>> getAllUserInfo() { return mAllUserInfo; }
    public LiveData<User> isFirstUser() { return mUser; }
    public LiveData<List<Activity>> getAllActivities() { return mActivities; }
    public LiveData<List<Goal>> getAllGoals() { return mGoals; }
    public LiveData<List<Steps>> getStepsBetween(Date start, Date end) { return mStepsRepo.getAllStepsBetween(start , end); }
    public LiveData<List<Goal>> getGoalsOfType(String goal_type) { return  mStepsRepo.getGoalsOfType(goal_type); }

    public void insertSteps(Steps steps) {
        mStepsRepo.insertSteps(steps);
    }
    public void insertUserInfo(User user_info) {
        mStepsRepo.insertUserInfo(user_info);
    }
    public void insertActivity(Activity activity) { mStepsRepo.insertActivity(activity); }
    public void insertGoal(Goal goal) { mStepsRepo.insertGoal(goal); }

    public void deleteGoal(Long id) { mStepsRepo.deleteGoal(id); }
    public void deleteActivity(Long id) { mStepsRepo.deleteActivity(id); }
}
