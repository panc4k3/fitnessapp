package com.app.quikfit.orm;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "activities")
public class Activity {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    @NonNull
    @ColumnInfo(name = "type")
    public String type;
    @NonNull
    @ColumnInfo(name = "calories")
    public int calories;
    @ColumnInfo(name = "image")
    public String image_name;
    @ColumnInfo(name = "date_created")
    @TypeConverters({Converters.class})
    public Date timestamp;

    public Activity(@NonNull String type, @NonNull int calories, String image_name)
    {
        this.type = type;
        this.calories = calories;
        this.image_name = image_name;
        this.timestamp = new Date();
    }

    public Long getId() { return this.id; }
    public String getActivityType() { return this.type; }
    public int getCalories() { return  this.calories; }
    public String getImageName() { return  this.image_name; }
    public Date getTimestamp() { return  this.timestamp; }
}
