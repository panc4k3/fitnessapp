package com.app.quikfit.orm;

import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.PrimaryKey;

import java.time.Instant;
import java.util.Date;

@Entity(tableName = "steps_taken")
public class Steps {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    @NonNull
    @ColumnInfo(name = "steps")
    public int mSteps;
    @NonNull
    @ColumnInfo(name = "date_created")
    @TypeConverters({Converters.class})
    public Date timestamp;

    public Steps(@NonNull int steps) {
        this.mSteps = steps;
        this.timestamp = new Date();
    }

    public int getSteps() {
        return this.mSteps;
    }
}
