package com.app.quikfit.orm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Dao
public interface StepsDao {

    //Steps table
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertSteps(Steps steps);

    @Query("DELETE FROM steps_taken")
    void deleteAllSteps();

    @Query("SELECT * FROM steps_taken ORDER BY date_created ASC")
    LiveData<List<Steps>> getAllSteps();

    @Query("SELECT * FROM steps_taken WHERE date_created BETWEEN :start AND :end ORDER BY date_created ASC")
    LiveData<List<Steps>> getAllStepsBetween(Date start, Date end);

    //User table
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertUserInfo(User user_info);

    @Query("DELETE FROM user")
    void deleteAllUserInfo();

    @Query("SELECT * FROM user ORDER BY date_created DESC")
    LiveData<List<User>> getAllUserInfo();

    @Query("SELECT * FROM user ORDER BY date_created DESC LIMIT 1")
    LiveData<User> isFirstUser();

    //Activities table
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertActivity(Activity activity);

    @Query("DELETE FROM activities")
    void deleteAllActivities();

    @Query("SELECT * FROM activities ORDER BY date_created DESC")
    LiveData<List<Activity>> getAllActivities();

    @Query("DELETE FROM activities WHERE id=:id")
    void deleteActivity(Long id);

    //Goals table
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertGoal(Goal goal);

    @Query("DELETE FROM goals")
    void deletleAllGoals();

    @Query("SELECT * FROM goals ORDER BY date_created DESC")
    LiveData<List<Goal>> getAllGoals();

    @Query("SELECT * FROM goals WHERE type = :goal_type ORDER BY date_created DESC")
    LiveData<List<Goal>> getGoalsOfType(String goal_type);

    @Query("DELETE FROM goals WHERE id=:id")
    void deleteGoal(Long id);

    @Update
    void updateGoal(Goal goal);
}
