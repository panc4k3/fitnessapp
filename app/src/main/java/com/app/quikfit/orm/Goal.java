package com.app.quikfit.orm;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Entity(tableName = "goals")
public class Goal {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    @NonNull
    @ColumnInfo(name = "type")
    public String type;
    @NonNull
    @ColumnInfo(name = "current_value")
    public int current;
    @NonNull
    @ColumnInfo(name = "goal_value")
    public int goal;
    @NonNull
    @ColumnInfo(name = "is_completed")
    public boolean completed;
    @NonNull
    @ColumnInfo(name = "date_created")
    @TypeConverters({Converters.class})
    public Date start_date;
    @NonNull
    @ColumnInfo(name = "date_to_complete")
    @TypeConverters({Converters.class})
    public Date end_date;

    public Goal(@NonNull String type, @NonNull int goal, @NonNull Date end_date) {
        this.type = type;
        this.current = 0;
        this.goal = goal;
        this.completed = false;
        Date date = new Date();
        this.start_date = date;
        this.end_date = end_date;
    }

    public Long getId() { return id; }
    public String getType() { return type; }
    public int getCurrentValue() { return current; }
    public int getGoalValue() { return goal; }
    public boolean isCompleted() { return completed; }
    public long getTimeLeft() { return !completed ? end_date.getTime() - start_date.getTime() : 0; }
}
