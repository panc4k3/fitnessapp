package com.app.quikfit.orm;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.sql.Date;

@Database(entities = {Steps.class, User.class, Activity.class, Goal.class}, version = 5, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class StepsRoomDb extends RoomDatabase {

    public abstract StepsDao stepsDao();

    private static volatile StepsRoomDb INSTANCE;

    static StepsRoomDb getDb(final Context context) {
        if (INSTANCE == null) {
            synchronized (StepsRoomDb.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), StepsRoomDb.class, "steps_db").fallbackToDestructiveMigration().addCallback(sRoomDatabaseCallback).build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final StepsDao mDao;

        PopulateDbAsync(StepsRoomDb db) {
            mDao = db.stepsDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
//            mDao.deleteAllSteps();
//            Goal goal = new Goal("Steps", 200, Date.valueOf("2019-05-05'T'10:33:45.000000"));
//            mDao.insertGoal(goal);
//            mDao.deleteAllUserInfo();
//            Steps steps = new Steps(5);
//            mDao.insertSteps(steps);
//            steps = new Steps(67);
//            mDao.insert(steps);
            return null;
        }
    }
}
