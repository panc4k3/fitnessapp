package com.app.quikfit.orm;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "user")
public class User {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    @NonNull
    public double height;
    @NonNull
    public double weight;
    @NonNull
    public boolean gender;
    @NonNull
    public boolean imperial;
    @NonNull
    public double stride;
    @NonNull
    public int sensitivity;
    @NonNull
    @ColumnInfo(name = "date_created")
    @TypeConverters({Converters.class})
    public Date timestamp;

    public User(@NonNull double height, @NonNull double weight, @NonNull boolean gender, @NonNull boolean imperial, @NonNull double stride, @NonNull int sensitivity) {
        this.height = height;
        this.weight = weight;
        this.gender = gender;
        this.imperial = imperial;
        this.stride = stride;
        this.sensitivity = sensitivity;
        this.timestamp = new Date();
    }

    public double getUserHeight() { return this.height; }
    public double getUserHeightImperial() { return Math.round(this.height / 2.54); }
    public double getUserWeight() { return this.weight; }
    public double getUserWeightImperial() { return Math.round(this.weight * 2.205); }
    public double getStrideLength() { return this.stride; }
    public double getStrideLengthImperial() { return Math.round(this.stride / 2.54); }
    public boolean getUserGender() { return this.gender; }
    public boolean isImperial() { return  this.imperial; }
    public int getSensitivity() { return  this.sensitivity; }
    public Date getTimestamp() { return  this.timestamp; }
}
