package com.app.quikfit.orm;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StepsRepository {

    private StepsDao mStepsDao;
    private LiveData<List<Steps>> mAllSteps;
    private LiveData<List<User>> mAllUserInfo;
    private LiveData<User> mUser;
    private LiveData<List<Activity>> mActivities;
    private LiveData<List<Goal>> mGoals;

    StepsRepository(Application application) {
        StepsRoomDb db = StepsRoomDb.getDb(application);
        mStepsDao = db.stepsDao();
        mAllSteps = mStepsDao.getAllSteps();
        mAllUserInfo = mStepsDao.getAllUserInfo();
        mUser = mStepsDao.isFirstUser();
        mActivities = mStepsDao.getAllActivities();
        mGoals = mStepsDao.getAllGoals();
    }

    LiveData<List<Steps>> getAllSteps() {
        return mAllSteps;
    }
    LiveData<List<User>> getAllUserInfo() { return  mAllUserInfo; }
    LiveData<User> isFirstUser() { return mUser; }
    LiveData<List<Activity>> getAllActivities() { return mActivities; }
    LiveData<List<Goal>> getAllGoals() { return mGoals; }
    LiveData<List<Steps>> getAllStepsBetween(Date start, Date end) { return mStepsDao.getAllStepsBetween(start, end); }
    LiveData<List<Goal>> getGoalsOfType(String goal_type) { return  mStepsDao.getGoalsOfType(goal_type); }

    public void insertSteps(Steps steps) {
        new insertStepsAsyncTask(mStepsDao).execute(steps);
    }
    public void insertUserInfo(User user_info) { new insertUserInfoAsyncTask(mStepsDao).execute(user_info); }
    public  void insertActivity(Activity activity) {new insertActivityAsyncTask(mStepsDao).execute(activity); }
    public void insertGoal(Goal goal) { new insertGoalAsyncTask(mStepsDao).execute(goal); }

    public void deleteGoal(Long id) { new deleteGoalAsyncTask(mStepsDao).execute(id); }
    public void deleteActivity(Long id) { new deleteActivityAsyncTask(mStepsDao).execute(id); }

    private static class insertStepsAsyncTask extends AsyncTask<Steps, Void, Void> {
        private StepsDao mAsyncTaskDao;

        insertStepsAsyncTask(StepsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Steps... params) {
            mAsyncTaskDao.insertSteps(params[0]);
            return null;
        }
    }

    private static class insertUserInfoAsyncTask extends AsyncTask<User, Void, Void> {
        private StepsDao mAsyncTaskDao;

        insertUserInfoAsyncTask(StepsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.insertUserInfo(params[0]);
            return null;
        }
    }

    private static class insertActivityAsyncTask extends AsyncTask<Activity, Void, Void> {
        private StepsDao mAsyncTaskDao;

        insertActivityAsyncTask(StepsDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(final Activity... params) {
            mAsyncTaskDao.insertActivity(params[0]);
            return null;
        }
    }

    private static class insertGoalAsyncTask extends AsyncTask<Goal, Void, Void> {
        private StepsDao mAsyncTaskDao;

        insertGoalAsyncTask(StepsDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(final Goal... params) {
            mAsyncTaskDao.insertGoal(params[0]);
            return null;
        }
    }

    private static class deleteGoalAsyncTask extends AsyncTask<Long, Void, Void> {
        private StepsDao mAsyncTaskDao;

        deleteGoalAsyncTask(StepsDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(final Long... params) {
            mAsyncTaskDao.deleteGoal(params[0]);
            return null;
        }
    }

    private static class deleteActivityAsyncTask extends AsyncTask<Long, Void, Void> {
        private StepsDao mAsyncTaskDao;

        deleteActivityAsyncTask(StepsDao dao) { mAsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(final Long... params) {
            mAsyncTaskDao.deleteActivity(params[0]);
            return null;
        }
    }
}
