package com.app.quikfit.ui.main;

import android.app.Application;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.quikfit.GraphActivity;
import com.app.quikfit.R;
import com.app.quikfit.orm.Goal;
import com.app.quikfit.orm.Steps;
import com.app.quikfit.orm.StepsViewModel;
import com.app.quikfit.orm.User;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class HomeFragment extends Fragment {

    private StepsViewModel stepsViewModel;
    private FirebaseAuth mAuth;

    TextView mUserEmail;
    PieChart chart;
    TextView mStepsTaken;
    TextView mCaloriesBurned;
    TextView mStepsTakenText;

    PieData mPieData;
    PieDataSet mPieDataSet;

    ArrayList<Entry> mPieEntries;
    ArrayList<String> mPieLabels;

    Goal mCurrentStepGoal;

    private int steps_goal = 1000;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        mAuth = FirebaseAuth.getInstance();

        mUserEmail = root.findViewById(R.id.account_email);
        chart = root.findViewById(R.id.image_graph);
        mStepsTaken = root.findViewById(R.id.steps_edit);
        mCaloriesBurned = root.findViewById(R.id.calories_edit);
        mStepsTakenText = root.findViewById(R.id.steps_taken);

        mUserEmail.setText(mAuth.getCurrentUser().getEmail());

        stepsViewModel.getAllSteps().observe(this, new Observer<List<Steps>>() {
            @Override
            public void onChanged(@Nullable List<Steps> steps) {
                int step_count = 0;
                for (Steps steps_item : steps) {
                    step_count += steps_item.getSteps();
                }
                DecimalFormat df = new DecimalFormat("0.00");
                mStepsTaken.setText(Integer.toString(step_count));
                mCaloriesBurned.setText(df.format(step_count * 0.05));
            }
        });

        mStepsTakenText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GraphActivity.class);
                intent.putExtra("GRAPH_TYPE", 1);
                startActivity(intent);
            }
        });

        stepsViewModel.getGoalsOfType(getResources().getStringArray(R.array.list_goal_types)[2]).observe(this, new Observer<List<Goal>>() {
            @Override
            public void onChanged(@Nullable List<Goal> goals) {
                if(goals.size() > 0) {
                    mCurrentStepGoal = goals.get(0);
                    steps_goal = mCurrentStepGoal.getGoalValue();
                }
            }
        });

        setGraphStepsDay();

        return root;
    }

    public void setGraphStepsDay() {
        mPieEntries = new ArrayList<>();
        mPieLabels = new ArrayList<>();

        Calendar start = Calendar.getInstance();
        start.add(Calendar.DATE, -1);

        stepsViewModel.getStepsBetween(start.getTime(), Calendar.getInstance().getTime()).observe(this, new Observer<List<Steps>>() {
            int mStepCount = 0;

            @Override
            public void onChanged(@Nullable List<Steps> steps) {
                for(int i = 0; i < steps.size(); i++) {
                    mStepCount += steps.get(i).getSteps();
                }

                mPieEntries.add(new Entry(mStepCount, 0));
                mPieEntries.add(new Entry(steps_goal - mStepCount, 1));

                mPieLabels.add("Steps Taken");
                mPieLabels.add("Steps Goal");

                mPieDataSet = new PieDataSet(mPieEntries, "Today");
                mPieData = new PieData(mPieLabels, mPieDataSet);
                mPieDataSet.setColors(new int[] {
                        getResources().getColor(R.color.color_one, getContext().getTheme()),
                        getResources().getColor(R.color.color_two, getContext().getTheme()),
                        getResources().getColor(R.color.color_three, getContext().getTheme()),
                        getResources().getColor(R.color.color_four, getContext().getTheme()),
                        getResources().getColor(R.color.color_five, getContext().getTheme())
                });

                chart.setData(mPieData);
                chart.animateY(2000);
                chart.setDescription("Steps Today");
                chart.getLegend().setEnabled(false);
            }
        });
    }
}
