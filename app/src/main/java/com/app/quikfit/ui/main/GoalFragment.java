package com.app.quikfit.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.app.quikfit.GoalActivity;
import com.app.quikfit.MainActivity;
import com.app.quikfit.R;
import com.app.quikfit.orm.Goal;
import com.app.quikfit.orm.StepsViewModel;

import java.util.List;

public class GoalFragment extends Fragment {

    StepsViewModel stepsViewModel;
    RecyclerView goalRecyclerView;

    Button mAddGoalActivity;

    private Goal selected_goal;

    public GoalFragment() {
        // Required empty public constructor
    }

    public static GoalFragment newInstance() {
        GoalFragment fragment = new GoalFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_goal, container, false);

        goalRecyclerView = root.findViewById(R.id.goal_list);
        goalRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        goalRecyclerView.setHasFixedSize(true);

        final GoalAdapter adapter = new GoalAdapter();
        goalRecyclerView.setAdapter(adapter);

        stepsViewModel.getAllGoals().observe(this, new Observer<List<Goal>>() {
            @Override
            public void onChanged(@Nullable List<Goal> goals) {
                adapter.setGoals(goals);
            }
        });

        mAddGoalActivity = root.findViewById(R.id.bt_add_goal);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                stepsViewModel.deleteGoal(adapter.getGoalAt(viewHolder.getAdapterPosition()).getId());
                Toast.makeText(getActivity(), "Goal Removed", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(goalRecyclerView);

        adapter.setOnItemClickListener(new GoalAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Goal goal) {
                //Implement onclick functionality for recyclerview item click
                selected_goal = goal;
            }
        });

        mAddGoalActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GoalActivity.class);
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        return root;
    }
}
