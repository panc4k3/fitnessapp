package com.app.quikfit.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.quikfit.GraphActivity;
import com.app.quikfit.LoginActivity;
import com.app.quikfit.R;
import com.app.quikfit.orm.Goal;
import com.app.quikfit.orm.StepsViewModel;
import com.app.quikfit.orm.User;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class SettingsFragment extends Fragment {

    private FirebaseAuth mAuth;

    StepsViewModel stepsViewModel;

    TextView mUserEmail;

    Button mChangeAccount;
    Button mUpdateDetails;

    Spinner mUnitType;

    Spinner mStrideUnitType;
    EditText mStrideLength;

    Spinner mStepSensitivity;

    Spinner mHeightUnitType;
    EditText mHeight;

    Spinner mWeightUnitType;
    EditText mWeight;
    TextView mWeightText;

    Spinner mGender;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_settings, container, false);

        mAuth = FirebaseAuth.getInstance();

        mUserEmail = root.findViewById(R.id.user_email);

        mChangeAccount = root.findViewById(R.id.bt_change_user);
        mUpdateDetails = root.findViewById(R.id.bt_settings_update);

        mUnitType = root.findViewById(R.id.unit_system_spinner);

        mStrideUnitType = root.findViewById(R.id.text_unit_scale);
        mStrideLength = root.findViewById(R.id.stride_length);

        mStepSensitivity = root.findViewById(R.id.step_sensitivity_spinner);

        mHeightUnitType = root.findViewById(R.id.text_height_scale);
        mHeight = root.findViewById(R.id.user_height_edit);

        mWeightUnitType = root.findViewById(R.id.text_weight_scale);
        mWeight = root.findViewById(R.id.user_weight_edit);
        mWeightText = root.findViewById(R.id.text_weight_settings);

        mGender = root.findViewById(R.id.user_gender_spinner);

        mUserEmail.setText(mAuth.getCurrentUser().getEmail());

        stepsViewModel.getAllUserInfo().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
//                Log.i("USER", "Imperial: " + users.get(0).isImperial());
                if(users.get(0).isImperial()) {
                    mUnitType.setSelection(1);
                    mHeight.setText(Double.toString(users.get(0).getUserHeightImperial()));
                    mHeightUnitType.setSelection(1);
                    mWeight.setText(Double.toString(users.get(0).getUserWeightImperial()));
                    mWeightUnitType.setSelection(1);
                    mStrideLength.setText(Double.toString(users.get(0).getStrideLengthImperial()));
                    mStrideUnitType.setSelection(1);
                } else {
                    mUnitType.setSelection(0);
                    mHeight.setText(Double.toString(users.get(0).getUserHeight()));
                    mHeightUnitType.setSelection(0);
                    mWeight.setText(Double.toString(users.get(0).getUserWeight()));
                    mWeightUnitType.setSelection(0);
                    mStrideLength.setText(Double.toString(users.get(0).getStrideLength()));
                    mStrideUnitType.setSelection(0);
                }

                mStepSensitivity.setSelection(users.get(0).getSensitivity());

                if(users.get(0).getUserGender()) {
                    mGender.setSelection(1);
                } else {
                    mGender.setSelection(0);
                }
            }
        });

        mUpdateDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                double height;
                double weight;
                double stride_length;
                boolean gender;
                boolean imperial;

                Log.i("SPINNER", "SPINNER: " + mUnitType.getSelectedItemId() + " == " + getResources().getStringArray(R.array.unit_system_list)[0]);

                if(mHeightUnitType.getSelectedItemId() == 0) {
                    height = Double.parseDouble(mHeight.getText().toString());
                } else {
                    height = Math.round(Double.parseDouble(mHeight.getText().toString()) * 2.54);
                }

                if(mWeightUnitType.getSelectedItemId() == 0) {
                    weight = Double.parseDouble(mWeight.getText().toString());
                } else {
                    weight = Math.round(Double.parseDouble(mWeight.getText().toString()) / 2.205);
                }

                if(mStrideUnitType.getSelectedItemId() == 0) {
                    stride_length = Double.parseDouble(mStrideLength.getText().toString());
                } else {
                    stride_length = Math.round(Double.parseDouble(mStrideLength.getText().toString()) * 2.54);
                }

                if(mGender.getSelectedItemId() == 0) {
                    gender = false;
                } else {
                    gender = true;
                }

                if(mUnitType.getSelectedItemId() == 0) {
                    imperial = false;
                } else {
                    imperial = true;
                }
                stepsViewModel.insertUserInfo(new User(height, weight, gender, imperial, stride_length, mStepSensitivity.getSelectedItemPosition()));
            }
        });

        mChangeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        mWeightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GraphActivity.class);
                intent.putExtra("GRAPH_TYPE", 0);
                startActivity(intent);
            }
        });

        return root;
    }
}
