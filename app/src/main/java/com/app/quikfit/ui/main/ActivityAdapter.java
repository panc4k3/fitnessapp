package com.app.quikfit.ui.main;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.quikfit.R;
import com.app.quikfit.orm.Activity;
import com.app.quikfit.orm.Goal;

import java.util.ArrayList;
import java.util.List;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityHolder> {

    private List<Activity> activities = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public ActivityHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View activityView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_item, viewGroup, false);
        return new ActivityHolder(activityView);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityHolder activityHolder, int i) {
        Activity currentActivity = activities.get(i);
        String activity_value = currentActivity.getCalories() + " " + currentActivity.getActivityType();

        Bitmap bitmap = BitmapFactory.decodeFile(currentActivity.getImageName());

        activityHolder.mActivityType.setText(currentActivity.getActivityType());
        activityHolder.mActivityValue.setText(activity_value);
        activityHolder.mActivityImage.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() { return activities.size(); }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
        notifyDataSetChanged();
    }

    public Activity getActivityAt(int position) { return activities.get(position); }

    class ActivityHolder extends RecyclerView.ViewHolder {
        private TextView mActivityType;
        private TextView mActivityValue;
        private ImageView mActivityImage;

        public ActivityHolder(View itemView) {
            super(itemView);

            mActivityType = itemView.findViewById(R.id.activity_item_type);
            mActivityValue = itemView.findViewById(R.id.activity_item_value);
            mActivityImage = itemView.findViewById(R.id.activity_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(activities.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Activity activity);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
