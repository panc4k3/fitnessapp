package com.app.quikfit.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.quikfit.ActivityActivity;
import com.app.quikfit.R;
import com.app.quikfit.orm.Activity;
import com.app.quikfit.orm.StepsViewModel;

import java.util.List;

public class ActivityFragment extends Fragment {

    StepsViewModel stepsViewModel;
    RecyclerView activityRecyclerView;

    Button mAddActivityActivity;

    private Activity selected_activity;

    public ActivityFragment() {
        // Required empty public constructor
    }

    public static ActivityFragment newInstance() {
        ActivityFragment fragment = new ActivityFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_activity, container, false);

        activityRecyclerView = root.findViewById(R.id.activity_list);
        activityRecyclerView.setLayoutManager((new LinearLayoutManager(this.getContext())));
        activityRecyclerView.setHasFixedSize(true);

        final ActivityAdapter adapter = new ActivityAdapter();
        activityRecyclerView.setAdapter(adapter);

        stepsViewModel.getAllActivities().observe(this, new Observer<List<Activity>>() {

            @Override
            public void onChanged(@Nullable List<Activity> activities) {
                adapter.setActivities(activities);
            }
        });

        mAddActivityActivity = root.findViewById(R.id.bt_add_activity);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                stepsViewModel.deleteActivity(adapter.getActivityAt(viewHolder.getAdapterPosition()).getId());
                Toast.makeText(getActivity(), "Activity Removed", Toast.LENGTH_LONG).show();
            }
        }).attachToRecyclerView(activityRecyclerView);

        adapter.setOnItemClickListener(new ActivityAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Activity activity) {
                selected_activity = activity;
            }
        });

        mAddActivityActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityActivity.class);
                startActivity(intent);
            }
        });
        // Inflate the layout for this fragment
        return root;
    }
}
