package com.app.quikfit.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.app.quikfit.R;
import com.app.quikfit.orm.Goal;

import java.util.ArrayList;
import java.util.List;

public class GoalAdapter extends RecyclerView.Adapter<GoalAdapter.GoalHolder> {

    private List<Goal> goals = new ArrayList<>();
    private  OnItemClickListener listener;

    @NonNull
    @Override
    public GoalHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View goalView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.goal_item, viewGroup, false);
        return new GoalHolder(goalView);
    }

    @Override
    public void onBindViewHolder(@NonNull GoalHolder goalHolder, int i) {
        Goal currentGoal = goals.get(i);
        String goal_value = currentGoal.getGoalValue() + " " + currentGoal.getType();
        int goal_progress = Float.floatToIntBits((float)currentGoal.getCurrentValue() / (float)currentGoal.getGoalValue() * 100);
        goalHolder.mGoalType.setText(currentGoal.getType());
        goalHolder.mGoalValue.setText(goal_value);
        goalHolder.mGoalPercent.setProgress(goal_progress);
    }

    @Override
    public int getItemCount() {
        return goals.size();
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
        notifyDataSetChanged();
    }

    public Goal getGoalAt(int position) {
        return goals.get(position);
    }

    class GoalHolder extends RecyclerView.ViewHolder {

        private TextView mGoalType;
        private TextView mGoalValue;
        private ProgressBar mGoalPercent;


        public GoalHolder(View itemView) {
            super(itemView);

            mGoalType = itemView.findViewById(R.id.goal_item_type);
            mGoalValue = itemView.findViewById(R.id.goal_item_value);
            mGoalPercent = itemView.findViewById(R.id.goal_item_percent);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(goals.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Goal goal);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
