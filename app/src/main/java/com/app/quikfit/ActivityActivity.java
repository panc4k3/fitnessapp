package com.app.quikfit;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.quikfit.orm.Activity;
import com.app.quikfit.orm.StepsViewModel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class ActivityActivity extends AppCompatActivity {

    StepsViewModel stepsViewModel;

    Spinner mActivityType;
    EditText mActivityCalories;
    ImageView mActivityImage;
    Button mConfirm;

    String pathToFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity);

        stepsViewModel = ViewModelProviders.of(this).get(StepsViewModel.class);

        mActivityType = findViewById(R.id.activity_type);
        mActivityCalories = findViewById(R.id.activity_value);
        mActivityImage = findViewById(R.id.activity_imageview);
        mConfirm = findViewById(R.id.bt_activity_confirm);

        requestPermissions(new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        mActivityImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchPictureTakerAction();
            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = new Activity(mActivityType.getSelectedItem().toString(), Integer.parseInt(mActivityCalories.getText().toString()), pathToFile);
                stepsViewModel.insertActivity(activity);
                Toast.makeText(ActivityActivity.this, "Activity Added", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if(requestCode == 1) {
                Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
                mActivityImage.setImageBitmap(bitmap);
            }
        }
    }

    private void dispatchPictureTakerAction() {
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(takePhoto.resolveActivity(getPackageManager()) != null) {
            File photoFile = createPhotoFile();

            if(photoFile != null) {
                pathToFile = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(ActivityActivity.this, "com.app.quikfit.ActivityActivity", photoFile);
                takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePhoto, 1);
            }
        }
    }

    private File createPhotoFile() {
        String name = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US).format(new Date());

        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;

        try {
            image = File.createTempFile(name, ".jpg", storageDir);
        } catch (Exception e) {
            Log.i("IMAGE","ERROR" ,e.fillInStackTrace());
        }
        return image;
    }
}
